<?php

session_start();

?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <?php
        
        $bdd = new PDO('mysql:host=localhost;dbname=wabeindustries', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

        $bdd_administrateur = $bdd->query('SELECT * FROM parking_users');
        while($administrateur = $bdd_administrateur->fetch()){
            if((!$_SESSION['identifiant'] == $administrateur['session'])){
                header("Location: ../../index.php");
            }
        };

        ?>
        <title>Parking | Dashboard</title>
        <meta charset="UTF-8"/>
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Lobster&family=Redressed&family=Roboto&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="../css/app.css"/>
        <link rel="shortcut icon" type="image/x-icon" href="../img/Icone.png" />
        <link rel="stylesheet" media="screen and (max-width: 1250px)" href="../css/tablet.css" />
        <link rel="stylesheet" media="screen and (max-width: 750px)" href="../css/phone.css" />
    </head>
    <div class="header">
        <a href="../../index.php" class="close-img"><img class="close-img" src="../img/logout.png" alt="Icone Logout"></a>
        <h1>Parking</h1>
    </div>
    <body>
        <div class="restante_1">
            <h2>Nombre de places restantes :</h2>
            <h3>145 / 200</h3>
        </div>
        <div class="restante">
            <h2>Nombre de places restantes :</h2>
            <h3>145 / 200</h3>
        </div>
    </body>
    <footer>
        <h5>© parking.tsti2d1.gq - 2021</h5>
    </footer>
    <script src="../js/AffichageMenu.js"></script>
</html>