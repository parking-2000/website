<body class="error_page">
    <div class="error_page">
        <h1 class="error_title">Parking - ERROR</h1>
        <h2 class="waberror">WabeError - 404</h2>
        <h3 class="error_p">Tu te trouve sur une page d'erreur, tu peux quitter cette page avec ces bouton !</h3>
        <a href="index.php"><button class="button_error">Retour au site</button></a>
    </div>
</body>