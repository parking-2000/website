<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Parking | Home</title>
        <meta charset="UTF-8"/>
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Lobster&family=Redressed&family=Roboto&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="resources/css/app.css"/>
        <link rel="shortcut icon" type="image/x-icon" href="resources/img/Icone.png" />
        <link rel="stylesheet" media="screen and (max-width: 1250px)" href="resources/css/tablet.css" />
        <link rel="stylesheet" media="screen and (max-width: 750px)" href="resources/css/phone.css" />
    </head>
    <div class="header">
        <a id="affichage-boutton" href="#"><img class="header-menu" src="resources/img/menu.png" alt="Icone Menu"/></a>
        <a href="mainpage.php"><h1>Parking</h1></a>
    </div>
    <?php include('resources/php/menu.php') ?>
    <?php include('resources/php/connexion-menu.php') ?>
    <body>
        <div class="main">
            <h1>Bienvenue sur le site du projet de :</h1>
            <h2>Parking en zone Natura 2000</h2>
            <img src="resources/img/Icone.png" alt="Image Parking"/>
        </div>
    </body>
    <script src="resources/js/AffichageMenu.js"></script>
    <script src="resources/js/Login.js"></script>
</html>