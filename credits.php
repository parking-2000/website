<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Parking | Crédits</title>
        <meta charset="UTF-8"/>
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Lobster&family=Redressed&family=Roboto&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="resources/css/app.css"/>
        <link rel="shortcut icon" type="image/x-icon" href="resources/img/Icone.png" />
        <link rel="stylesheet" media="screen and (max-width: 1250px)" href="resources/css/tablet.css" />
        <link rel="stylesheet" media="screen and (max-width: 750px)" href="resources/css/phone.css" />
    </head>
    <div class="header">
        <a id="affichage-boutton" href="#"><img class="header-menu" src="resources/img/menu.png" alt="Icone Menu"/></a>
        <h1>Crédits</h1>
    </div>
    <?php include('resources/php/menu.php') ?>
    <?php include('resources/php/connexion-menu.php') ?>
    <body>
        <div class="credits">
            <h1>Pour faire ce projet, nous étions dans un groupe de 5 personnes :</h1>
            <h2>VILLEMAGNE Célia</h2>
            <h2>DESBOIS Enzo</h2>
            <h2>POMATHIOD Mathis</h2>
            <h2>VALVERDE Florian</h2>
            <h2>MILLAN Romain</h2>
        </div>
    </body>
    <footer>
        <h6>© parking.tsti2d1.gq - 2021</h6>
    </footer>
    <script src="resources/js/AffichageMenu.js"></script>
    <script src="resources/js/Login.js"></script>
</html>