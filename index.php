<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Parking | InDev</title>
        <meta charset="UTF-8"/>
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Lobster&family=Redressed&family=Roboto&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="resources/css/app.css"/>
        <link rel="shortcut icon" type="image/x-icon" href="resources/img/Icone.png" />
        <link rel="stylesheet" media="screen and (max-width: 1250px)" href="resources/css/tablet.css" />
        <link rel="stylesheet" media="screen and (max-width: 750px)" href="resources/css/phone.css" />
    </head>
    <div class="header">
        <center><a href="index.php"><h1>Parking</h1></a></center>
    </div>
    <body>
        <div class="indev">
            <h1>Parking en zone Natura 2000</h1>
            <h2>Ce site est encore en développement, des bugs sont alors à prévoir</h2>
            <a href="mainpage.php"><button>Indev - 0.99.8</button></a>
        </div>
    </body>
    
    <footer>
        <h6>© parking.tsti2d1.gq - 2021</h6>
    </footer>
    <script src="resources/js/AffichageMenu.js"></script>
    <script src="resources/js/Login.js"></script>
</html>