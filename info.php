<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Parking | Informations</title>
        <meta charset="UTF-8"/>
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Lobster&family=Redressed&family=Roboto&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="resources/css/app.css"/>
        <link rel="shortcut icon" type="image/x-icon" href="resources/img/Icone.png" />
        <link rel="stylesheet" media="screen and (max-width: 1250px)" href="resources/css/tablet.css" />
        <link rel="stylesheet" media="screen and (max-width: 750px)" href="resources/css/phone.css" />
    </head>
    <div class="header">
        <a id="affichage-boutton" href="#"><img class="header-menu" src="resources/img/menu.png" alt="Icone Menu"/></a>
        <h1>Information</h1>
    </div>
    <?php include('resources/php/menu.php') ?>
    <?php include('resources/php/connexion-menu.php') ?>
    <body>
        <div class="info">
            <h2 id="title">Ce projet est une conception d'un système de gestion du stationnement des véhicules en zone classée et protégée</h2>
            <h2 class="subtitle">L'énoncée</h2>
            <h3 id="enoncee-para">Vus que la circulation des véhicules à moteurs, en dehors des voies ouvertes, sont la cause des dommages aux millieux naturels, alors une réglementation à était mise en place</h3>
            <h3 class="subtitle">La Problématique</h3>
            <h3 class="problem-para">Vus que le parking haut est limité en nombres de place, il faut alors gérer le flux de voiture montantes, pour limitée les voitures le long des chemins d'accès</h3>
            <h3 class="problem-para">Mais comment mettre en place un telle système ?</h3>
            <h3 class="subtitle">La Réponse</h3>
            <h3 class="rep-para-title">Nous allons alors formée 5 parties qui sont alors :</h3>
            <h3 class="rep-part">Acquérir, Mémoriser et Transmettre le taux d'occupation du parking</h3>
            <h3 class="rep-part">Acquérir les données du taux d'occupation du parking</h3>
            <h3 class="rep-part">Acquérir, Mémoriser et Visualiser l'autorisation d'accès des véhicules prioritaires</h3>
            <h3 class="rep-part">Visualiser le nombre des places restantes</h3>
            <h3 class="rep-part">Commander La Manœuvre De La Barrière Et Acquérir La Hauteur Des Véhicules</h3>
            <h4 class="info-final">Voila, maintenant au travail</h4>
        </div>
    </body>
    <footer>
        <h6>© parking.tsti2d1.gq - 2021</h6>
    </footer>
    <script src="resources/js/AffichageMenu.js"></script>
    <script src="resources/js/Login.js"></script>
</html>